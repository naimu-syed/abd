package in.co.rshop.ems.dto;

import java.util.List;

public class ResponseDto {

	private String message;
	private Integer code;
	private List<Object> data;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public List<Object> getData() {
		return data;
	}

	public void setData(List<Object> data) {
		this.data = data;
	}

	public ResponseDto() {
		super();
	}

	public ResponseDto(String message, Integer code, List<Object> data) {
		super();
		this.message = message;
		this.code = code;
		this.data = data;
	}

}
