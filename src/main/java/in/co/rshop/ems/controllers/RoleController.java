package in.co.rshop.ems.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import in.co.rshop.ems.entities.Role;
import in.co.rshop.ems.services.RoleService;

@RestController
@RequestMapping("/api/roles")
public class RoleController {

	@Autowired
	private RoleService roleService;

	@GetMapping("/findallroles")
	public List<Role> getAllRoles() {
		return roleService.getAllRoles();
	}

	@GetMapping("/getuserbyid/{id}")
	public ResponseEntity<Role> getRoleById(@PathVariable Long id) {
		Role role = roleService.getRoleById(id);
		if (role != null) {
			return ResponseEntity.ok(role);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
}
