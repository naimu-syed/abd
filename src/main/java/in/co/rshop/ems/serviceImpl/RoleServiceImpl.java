package in.co.rshop.ems.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.co.rshop.ems.entities.Role;
import in.co.rshop.ems.repositories.RoleRepo;
import in.co.rshop.ems.services.RoleService;

@Service
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleRepo roleRepository;

	@Override
	public List<Role> getAllRoles() {
		List<Role> findAll = roleRepository.findAll();
		return findAll;
	}

	@Override
	public Role getRoleById(Long id) {

		Optional<Role> optionalRole = roleRepository.findById(id);
		return optionalRole.orElse(null);
	}

}
