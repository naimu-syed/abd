package in.co.rshop.ems.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.co.rshop.ems.entities.User;
import in.co.rshop.ems.repositories.UserRepo;
import in.co.rshop.ems.services.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepo userRepository;

	@Override
	public List<User> getAllUsers() {
		return userRepository.findAll();
	}

	@Override
	public User getUserById(Long id) {
		Optional<User> optionalUser = userRepository.findById(id);
		return optionalUser.orElse(null);
	}

	@Override
	public User createUser(User user) {
		User findByEmail = userRepository.findByEmail(user.getEmail());
		if (findByEmail != null)
			return user;
		return userRepository.save(user);
	}

	@Override
	public User updateUser(Long id, User user) {
		if (userRepository.existsById(id)) {
			user.setId(id);
			return userRepository.save(user);
		} else {
			return null;
		}
	}

	@Override
	public boolean deleteUser(Long id) {
		if (userRepository.existsById(id)) {
			userRepository.deleteById(id);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public User loginUser(User user) {

		User findByEmailAndPassword = userRepository.findByEmailAndPassword(user.getEmail(), user.getPassword());
		return findByEmailAndPassword;
	}
}
