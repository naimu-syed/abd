package in.co.rshop.ems.entities;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Feedback {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String description;
	private LocalDate date;

	@Transient
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "feedback")
	@JsonIgnore
	private User user;

	@Transient
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "feedback")
	@JsonIgnore
	private EducationalInstitution educationalInstitution;

	@Transient
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "feedback")
	@JsonIgnore
	private Course course;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public EducationalInstitution getEducationalInstitution() {
		return educationalInstitution;
	}

	public void setEducationalInstitution(EducationalInstitution educationalInstitution) {
		this.educationalInstitution = educationalInstitution;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Feedback() {
		super();
		// TODO Auto-generated constructor stub
	}

}
