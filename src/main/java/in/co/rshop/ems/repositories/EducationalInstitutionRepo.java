package in.co.rshop.ems.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import in.co.rshop.ems.entities.EducationalInstitution;

@Repository
public interface EducationalInstitutionRepo extends JpaRepository<EducationalInstitution, Long>{

}
