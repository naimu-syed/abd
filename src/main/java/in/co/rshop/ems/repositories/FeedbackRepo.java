package in.co.rshop.ems.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import in.co.rshop.ems.entities.Feedback;

@Repository
public interface FeedbackRepo extends JpaRepository<Feedback, Long> {

}
