package in.co.rshop.ems.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import in.co.rshop.ems.entities.User;

@Repository
public interface UserRepo extends JpaRepository<User, Long> {
	User findByEmail(String email);

	User findByEmailAndPassword(String email, String password);

}
