package in.co.rshop.ems.services;

import java.util.List;

import in.co.rshop.ems.entities.User;

public interface UserService {
	List<User> getAllUsers();

	User getUserById(Long id);

	User createUser(User user);

	User loginUser(User user);

	User updateUser(Long id, User user);

	boolean deleteUser(Long id);
}
