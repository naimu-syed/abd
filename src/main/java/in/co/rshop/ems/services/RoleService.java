package in.co.rshop.ems.services;

import java.util.List;

import in.co.rshop.ems.entities.Role;

public interface RoleService {

	List<Role> getAllRoles();

	Role getRoleById(Long id);
}
